var currentBase = 'dec'; //bin oct dec hex  // yeah global


/* Behaviour of button pressed
 * 
 * */
function pulsacion(n) {
    console.log('pressed button number: ' + n);
	var number = document.getElementById('Entrada').innerHTML;
    var originalBase = getBase(currentBase);
    
    
	switch(n) {
		case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
        case 10:
        case 11:
        case 12:
        case 13:
        case 14:
        case 15:
            document.getElementById('Entrada').innerHTML = appendDigit(currentBase, number, n);
		    break;
        case 16:
            document.getElementById('Entrada').innerHTML = appendPoint(number);
            break;
        case 17:
            document.getElementById('Entrada').innerHTML = deleteDigit(number);
            break;
		case 18:
            document.getElementById('Entrada').innerHTML = changeBase(number, originalBase, 2);
            setBin();
		    break;
		case 19:
		    document.getElementById('Entrada').innerHTML = changeBase(number, originalBase, 8);
            setOct();
		break;
		case 20:
            document.getElementById('Entrada').innerHTML = changeBase(number, originalBase, 10);
            setDec();
		    break;
		case 21:
			document.getElementById('Entrada').innerHTML = changeBase(number, originalBase, 16);
            setHex();
		    break;
	}
    resizeEntrada();
}

function getDigits()
{
    return Array('0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F');
}

function getLowerButtons()
{
    return Array('B0','B1','B2','B3','B4','B5','B6','B7','B8','B9','BA','BB','BC','BD','BE','BF', 'BPunto', 'BBorrar');
}

function getBase(name)
{
    if (name == 'bin') {
        return 2;
    }
    if (name == 'oct') {
        return 8;
    }
    if (name == 'dec') {
        return 10;
    }
    if (name == 'hex') {
        return 16;
    }
}


function getUpperButtons()
{
    return upperButtons = Array('bBIN','bOCT','bDEC','bHEX');
}

function getBaseButtons(base)
{
    var aux = ['BPunto', 'BBorrar'];
    var i, max;
    var dig = getDigits();
    
    max = 1;
    if (base == 'oct') {
        max = 7;
    }
    if (base == 'dec') {
        max = 9;
    }
    if (base == 'hex') {
        max = 15;
    }
    
    for (i = 0; i <= max; i++) {
        aux.push('B' + dig[i]);
    }
    return aux;
}



function appendDigit(base, number, value) {
    var DIG = getDigits();
    if (base == 'bin' && value > 1) {
        return number;
    }
    if (base == 'oct' && value > 7) {
        return number;
    }
    if (base == 'dec' && value > 9) {
        return number;
    }
    if (base == 'hex' && value > 15) {
        return number;
    }
    return number += DIG[value];
}


// add point if is valid input
function appendPoint(number)
{
    if (number.indexOf('.') >= 0) {
        return number;
    }
    return number += '.';
}

//delete las digit
function deleteDigit(number)
{
    return number.substr(0, number.length - 1);
}

/* Copy from template
 * Resize font
 * */

function resizeEntrada() {
    var aux = document.getElementById('Entrada');
	var aux2 = document.getElementById('cEntrada');
	
	if (aux.innerHTML !="") {
		var fontSize = 10;
		
        do {
            fontSize++; 
            aux.style.font='bold '+fontSize+'px "Courier New", Courier, monospace';
        } while (aux2.scrollHeight <= aux2.clientHeight && aux2.scrollWidth <= aux2.clientWidth);
    
        do {
            fontSize--; 
            aux.style.font='bold '+fontSize+'px "Courier New", Courier, monospace';
        } while (aux2.scrollHeight > aux2.clientHeight && aux2.scrollWidth > aux2.clientWidth);
        fontSize -=2;
        aux.style.font='bold '+fontSize+'px "Courier New", Courier, monospace';
        aux.style.top=(aux2.clientHeight - aux.clientHeight)/2 + 'px';
    }
}


function disableButtons(buttonIds)
{
    var i;
    var l = buttonIds.length;
    var buttonIds = buttonIds;
    
    for (i = 0; i < l ; i++) {
        document.getElementById(buttonIds[i]).style.opacity = "0.2";
        
    }
}

function enableButtons(buttonIds)
{
    var i;
    var l = buttonIds.length;
    var buttonIds = buttonIds;
    
    for (i = 0; i < l ; i++) {
        document.getElementById(buttonIds[i]).style.opacity = "1";
    }
}

function setUpperStyle(baseName)
{
    var buttonIds = getUpperButtons();
    var i;
    var l = buttonIds.length;
    for (i = 0; i < l ; i++) {
        document.getElementById(buttonIds[i]).style.backgroundColor = 'Black';
    }
    if (baseName == 'bin') {
        document.getElementById('bBIN').style.backgroundColor = '#80C0FF';
    }

    if (baseName == 'dec') {
        document.getElementById('bDEC').style.backgroundColor = '#80C0FF';
    }
    if (baseName == 'oct') {
        document.getElementById('bOCT').style.backgroundColor = '#80C0FF';
    }

    if (baseName == 'hex') {
        document.getElementById('bHEX').style.backgroundColor = '#80C0FF';
    }

}
	

window.onresize = function() {
	resizeEntrada();
}


function setBin() {
    setUpperStyle('bin');
    disableButtons(getLowerButtons());
    enableButtons(getUpperButtons());
    enableButtons(getBaseButtons('bin'));
    disableButtons(['bBIN']);
    
    currentBase = 'bin';
}

function setOct() {
    setUpperStyle('oct');
    disableButtons(getLowerButtons());
    enableButtons(getUpperButtons());
    enableButtons(getBaseButtons('oct'));
    disableButtons(['bOCT']);
    
    currentBase = 'oct';
}

function setDec() {
    setUpperStyle('dec');
    disableButtons(getLowerButtons());
    enableButtons(getUpperButtons());
    enableButtons(getBaseButtons('dec'));
    disableButtons(['bDEC']);
	
    currentBase = 'dec';
}

function setHex() {
    setUpperStyle('hex');
    disableButtons(getLowerButtons());
    enableButtons(getUpperButtons());
    enableButtons(getBaseButtons('hex'));
    disableButtons(['bHEX']);
    
    currentBase = 'hex';
}


//from http://coderstoolbox.net/number/
var regexps = {
    2  : /^([-+]?[01]*)(\.[01]*)?$/,
    8  : /^([-+]?[0-7]*)(\.[0-7]*)?$/,
    10 : /^([-+]?\d*)(\.\d*)?$/,
    16 : /^([-+]?[0-9a-f]*)(\.[0-9a-f]*)?$/i };
    

// yeah, from http://coderstoolbox.net/number/
function changeBase(number, fromBase, toBase) 
{
	var base = fromBase;
	var s = number;
    
    if (s == '') {
        return '';
    }
	// Allow 0x in front of hex numbers
	if (base == 16 && s.substr(0, 2) == '0x') {
		s = s.substr(2);
	}
	s = s.replace(/^ +| +$/g, '');

	var n;
	var matches = s.match(regexps[base]);
	if (!matches /*/^[-+]?\w*(\.\w*)?$/.test(s) */) {
	    n = NaN;
	} else if (!matches[2] || matches[2].length < 2) {
		n = parseInt('0' + matches[1], base);
	} else {
		n = parseInt('0' + matches[1], base);
		n += (matches[1].substr(0, 1) == '-' ? -1 : +1) * parseInt(matches[2].substr(1), base) / Math.pow(base, matches[2].length - 1);
	}
	// FIXME: check for invalid characters, that are silently ignored by parseInt()
	var bases = [2, 8, 10, 16];
    var i = bases.indexOf(toBase);
    var output;
    if (isNaN(n)) {
        output = 'NAN';
    } else if (16.25.toString(16) == '10.4') {
        // Opera 9 does not support toString() for floats with base != 10
        output = n.toString(bases[i]);
    } else {
        output = (n > 0 ? Math.floor(n) : Math.ceil(n)).toString(bases[i]);
        if (n % 1) {
            output += '.' + Math.round((Math.abs(n) % 1) * Math.pow(bases[i], 8)).toString(bases[i]);
            output = output.replace(/0+$/, '');
        }
    }
    return output.toUpperCase();
}

window.onload = function() {
    setDec();
}